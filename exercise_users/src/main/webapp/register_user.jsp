<%@ page import="pl.sda.jsp.exercise.User" %>
<%@ page import="pl.sda.jsp.exercise.GENDER" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.LocalDate" %><%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 25.09.2018
  Time: 18:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Action Register User</title>
</head>
<body>

<jsp:include page="header.jsp"></jsp:include>
<%! int licznik = 0; %>
<%
    User u = new User();
    u.setId(licznik++); // 0, 1, 2...


    u.setFirstName(request.getParameter("firstName"));
    GENDER gender = GENDER.valueOf(request.getParameter("gender"));
    u.setGender(gender);
    u.setLastName(request.getParameter("lastName"));
    u.setAddress(request.getParameter("address"));
    u.setHeight(Integer.parseInt(request.getParameter("height")));
    u.setBirthDate(LocalDate.parse(request.getParameter("birthdate")));
    u.setJoinDate(LocalDateTime.now());
    u.setUserName(request.getParameter("username"));


    List<User> users;
    if(session.getAttribute("user_list") != null){
    users = (List<User>) session.getAttribute("user_list");
    }else {
        users = new ArrayList<>();
    }

    users.add(u);

    session.setAttribute("user_list", users);
    response.sendRedirect("user_list.jsp");



    %>

</body>
</html>
