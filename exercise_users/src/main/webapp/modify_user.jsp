<%@ page import="pl.sda.jsp.exercise.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="pl.sda.jsp.exercise.GENDER" %>
<%@ page import="java.time.LocalDate" %><%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 25.09.2018
  Time: 20:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Modify User</title>
</head>
<body>

    <%
        List<User> users;
        if(session.getAttribute("user_list") != null){
            users = (List<User>) session.getAttribute("user_list");
        }else {
            users = new ArrayList<>();
        }

        String user_id = request.getParameter("modified_id");

        User searched = null;
        if(user_id != null){
            Long modifiedId = Long.parseLong(user_id);

            Iterator<User> it =users.iterator();
            while (it.hasNext()){
                User user = it.next();

                if(user.getId() == modifiedId ){
                    searched = user;
                    it.remove();
                    break;
                }
            }


        }

        searched.setUserName(request.getParameter("username"));
        searched.setFirstName(request.getParameter("firstName"));
        GENDER gender = GENDER.valueOf(request.getParameter("gender"));
        searched.setGender(gender);
        searched.setLastName(request.getParameter("lastName"));
        searched.setAddress(request.getParameter("address"));
        searched.setHeight(Integer.parseInt(request.getParameter("height")));
        searched.setBirthDate(LocalDate.parse(request.getParameter("birthdate")));
        searched.setUserName(request.getParameter("username"));

        users.add(searched);
        session.setAttribute("user_list", users);
        response.sendRedirect("user_list.jsp");
    %>

</body>
</html>
