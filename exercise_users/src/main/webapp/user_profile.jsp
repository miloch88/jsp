<%@ page import="pl.sda.jsp.exercise.User" %>
<%@ page import="pl.sda.jsp.exercise.GENDER" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.LocalDate" %><%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 24.09.2018
  Time: 20:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User Profile</title>
</head>
<body>
<%! int licznik = 0; %>
<%
    User user = new User();

    user.setId(licznik++);

    Character name = request.getParameter("firstName").charAt(0);
    if(Character.isUpperCase(name)){
            user.setFirstName(request.getParameter("firstName"));
    }else{
        response.sendRedirect("register_form.jsp?firstName=" + request.getParameter("firstName"));
    }

    Character surname = request.getParameter("lastName").charAt(0);
    if(Character.isUpperCase(surname)){
        user.setLastName(request.getParameter("lastName"));
    }else{
        response.sendRedirect("register_form.jsp");
    }

    if(request.getParameter("address").isEmpty()){
        response.sendRedirect("register_form.jsp");
    }else{
    user.setAddress(request.getParameter("address"));
    }

//    boolean isOK = true;
//    if(user.getHeight() < 60 || user.getHeight() > 230){
//        isOK = false;
//    }
//
////    if(user.getUserName().contains(" ")){
////        isOK = false;
////    }
//
//    if(!isOK){
//        response.sendRedirect("register_form.jsp");
//    }

    user.setUserName(request.getParameter("username"));
    user.setBirthDate(LocalDate.parse(request.getParameter("birthdate")));
    user.setJoinDate(LocalDateTime.now());
    user.setGender(GENDER.valueOf(request.getParameter("gender")));
    user.setHeight(Integer.parseInt(request.getParameter("height")));


//    out.print("<table>");
//
//    out.print("<tr> <td> Numer Id: </td> <td> " + user.getId() + " </td> </tr>");
//    out.print("<tr> <td> Imię: </td> <td> " + user.getFirstName() + " </td> </tr>");
//    out.print("<tr> <td> Nazisko: </td> <td> " + user.getLastName() + " </td> </tr>");
//    out.print("<tr> <td> Username: </td> <td> " + user.getUserName() + " </td> </tr>");
//    out.print("<tr> <td> Data urodzin: </td> <td> " + user.getBirthDate() + " </td> </tr>");
//    out.print("<tr> <td> Data założenia konta: </td> <td> " + user.getJoinDate() + " </td> </tr>");
//    out.print("<tr> <td> Płeć: </td> <td> " + user.getGender() + " </td> </tr>");
//    out.print("<tr> <td> Wzrost: </td> <td> " + user.getHeight() + " </td> </tr>");
//
//    out.print("</table>");

%>

<table>

    <thead > <th width="200" align="left"> Nazwa Parametru </th> <th width="200" align="left"> Wartość </th> </thead>
    <tr> <td> ID: </td> <td> <%= user.getId()%>  </td> </tr>
    <tr> <td> First name: </td> <td> <%= user.getFirstName()%>  </td> </tr>
    <tr> <td> Last name: </td> <td> <%= user.getLastName()%>  </td> </tr>
    <tr> <td> Username: </td> <td> <%= user.getUserName()%>  </td> </tr>
    <!-- Tutaj powinno być parsowanie-->
    <tr> <td> Birthdate: </td> <td> <%= user.getBirthDate()%>  </td> </tr>
    <tr> <td> Join date: </td> <td> <%= user.getJoinDate()%>  </td> </tr>
    <tr> <td> Gender: </td> <td> <%= user.getGender()%>  </td> </tr>
    <tr> <td> Height: </td> <td> <%= user.getHeight()%>  </td> </tr>


</table>


</body>
</html>
