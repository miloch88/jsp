<%@ page import="pl.sda.jsp.exercise.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="pl.sda.jsp.exercise.GENDER" %><%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 24.09.2018
  Time: 20:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Formularz: </title>
</head>
<body>


<%

    List<User> users;
    if(session.getAttribute("user_list") != null){
        users = (List<User>) session.getAttribute("user_list");
    }else {
        users = new ArrayList<>();
    }

    String user_id = request.getParameter("user_id");
        boolean female = true;

    User searched = null;
    if(user_id != null){
        Long modifiedId = Long.parseLong(user_id);

        Iterator<User> it =users.iterator();
        while (it.hasNext()){
            User user = it.next();

            if(user.getId() == modifiedId ){
                searched = user;
                break;

            }
        }

        if(searched != null){
            female = searched.getGender() == GENDER.FEMALE;
        }
    }
//    String firstName = request.getParameter("firstName");
//    if (firstName == null) {
//        firstName = "";
//    }
%>


<form action="<%= searched == null? "register_user.jsp" : "modify_user.jsp" %>" method="get" >
    <input type="hidden" hidden value="<%= searched == null ? "" : searched.getId()%>" name="modified_id">

    <h2>Formularz:</h2>

    <div>
        <label for="firstName">Name: </label>
        <input id="firstName" name="firstName" type="text" value="<%= searched == null ? "": searched.getFirstName() %>">
    </div>
    <div>
        <label for="lastName">Surname: </label>
        <input id="lastName" name="lastName" type="text" value="<%= searched == null ? "": searched.getLastName() %>">
    </div>
    <div>
        <label for="username">Username: </label>
        <input id="username" name="username" type="text" value="<%= searched == null ? "": searched.getUserName() %>">
    </div>

    <div>
        <label for="birthdate">Birthdate: </label>
        <input id="birthdate" name="birthdate" type="date" value="<%= searched == null ? "": searched.getBirthDate() %>">
    </div>

    <div>
        <label for="address">Address: </label>
        <input id="address" name="address" type="text" value="<%= searched == null ? "": searched.getAddress() %>">
    </div>

    <div id="gender">
        <label for="gender">Gender: </label>
        <input name="gender" type="radio" value="MALE" <%= female ? "" : "checked" %>>MALE</input>
        <input name="gender" type="radio" value="FEMALE" <%= female ? "checked" : "" %>>FEMALE</input>
    </div>

    <div>
        <label for="height">Height: </label>
        <input id="height" name="height" type="number" min="60" max="230" value="<%= searched == null ? "": searched.getHeight() %>">
    </div>

    <input type="submit" value="Wygenreuj">

</form>


</body>
</html>
