package pl.sda.jsp.exercise;

import java.sql.BatchUpdateException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class User {

    private long id;
    private String firstName;
    private String lastName;
    private String userName;
    private LocalDate birthDate;
    private LocalDateTime joinDate; //obecna data
    private String address;
    private int height;
    private GENDER gender;

    public User() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public LocalDateTime getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(LocalDateTime joinDate) {
        this.joinDate = joinDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public GENDER getGender() {
        return gender;
    }

    public void setGender(GENDER gender) {
        this.gender = gender;
    }

    public String toSerializedLine(){
        StringBuilder builder = new StringBuilder();

        builder.append(id).append(";");
        builder.append(firstName).append(";");
        builder.append(lastName).append(";");
        builder.append(userName).append(";");
        builder.append(birthDate).append(";");
        builder.append(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm").format(joinDate)).append(";");
        builder.append(address).append(";");
        builder.append(height).append(";");
        builder.append(gender).append(";");

        return builder.toString();
    }

    public void loadFromSerializedLine(String line){
        String[] value = line.split(";");
        setId(Long.parseLong(value[0]));
        setFirstName(value[1]);
        setLastName(value[2]);
        setUserName(value[3]);
        setBirthDate(LocalDate.parse(value[4]));
        setJoinDate(LocalDateTime.parse(value[5], DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")));
        setAddress((value[6]));
        setHeight(Integer.parseInt(value[7]));
        setGender(GENDER.valueOf(value[8]));

    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userName='" + userName + '\'' +
                ", birthDate=" + birthDate +
                ", joinDate=" + joinDate +
                ", address='" + address + '\'' +
                ", height=" + height +
                ", gender=" + gender +
                '}';
    }
}
