<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 03.10.2018
  Time: 20:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User list</title>
</head>
<body>
<h2>User List</h2>
<hr>
<table style="width: 100%; border: solid 1px #000000">
    <thead align="left">
    <th>Id</th>
    <th>Login</th>
    <th>Password</th>
    <th>Action remove</th>
    <th>Action modify</th>
    </thead>
    <tbody>
    <c:forEach var="user" items="${userList}">
        <tr>
            <td>
                <c:out value="${user.id}"></c:out>
            </td>
            <td>
                <c:out value="${user.login}"></c:out>
            </td>
            <td>
                <c:out value="${user.password}"></c:out>
            </td>
            <td>
                <a href="/user/remove?id=<c:out value="${user.id}"/>">Remove</a>
            </td>
            <td>
                <a href="/user/modify?id=<c:out value="${user.id}"/>">Modify</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<tr>
    <td>
        <form action="/user/add">
            <input type="submit" value="Add New User">
        </form>
    </td>
</tr>
</body>
</html>
