<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 04.10.2018
  Time: 20:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>TASK LIST</title>
</head>
<body>
<h2>TASK LIST</h2>
<hr>
<table style="width: 100%; border: solid 1px #000000">
    <thead align="left">

    <th>Id</th>
    <th>Title</th>
    <th>Content</th>
    <th>Done</th>
    <th></th>
    <th></th>
    </thead>

    <tbody>
    <%--Musi pobrać taskList z TaskListController--%>
    <c:forEach var="task" items="${taskList}">

        <tr>
            <td>
                <c:out value="${task.id}"></c:out>
            </td>
            <td>
                <c:out value="${task.title}"></c:out>
            </td>
            <td>
                <c:out value="${task.content}"></c:out>
            </td>
            <td>
                <c:out value="${task.done}"></c:out>
            </td>
            <td>
                <a href="/task/modify?id=<c:out value="${task.id}"/>">Modify</a>
            </td>
            <td>
                <a href="/task/remove?id=<c:out value="${task.id}"/>">Remove</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<tr>
    <td>
        <form action="/task/add">
            <input type="submit" value="Add New Task">
        </form>
    </td>
</tr>
</body>
</html>
