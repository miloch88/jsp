<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 04.10.2018
  Time: 20:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>TASK FORM</title>
</head>
<body>
<h2> TASK FORM:</h2>
<hr>
<span style="color: orange">
<c:out value="${error_message}"/>
</span>

<c:choose>

    <c:when test="${empty task_to_modify}">
        <form action="/task/add" method="post">
            <table>
                <tr>
                    <div>
                        <td>
                            <label for="title">Title: </label>
                        </td>
                        <td>
                            <input id="title" name="title" type="text">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div>
                        <td>
                            <label for="content">Content: </label>
                        </td>
                        <td>
                            <input id="content" name="content" type="text">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div>
                        <td>
                            <label for="done">Done: </label>
                        </td>
                        <td>
                            <input id="done" name="done" type="checkbox">
                        </td>
                    </div>
                </tr>
                <tr>
                    <td>
                        <div>
                            <input type="submit" value="Add">
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </c:when>

    <c:otherwise>
        <form action="/task/modify" method="post">
            <input type="hidden" name="id" value="<c:out value="${task_to_modify.id}"/>">
            <table>
                <tr>
                    <div>
                        <td>
                            <label for="title">Title: </label>
                        </td>
                        <td>
                            <input id="title" name="title" type="text"
                                   value="<c:out value="${task_to_modify.title}"/>">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div>
                        <td>
                            <label for="content">Content: </label>
                        </td>
                        <td>
                            <input id="content" name="content" type="text"
                                   value="<c:out value="${task_to_modify.content}"/>">
                        </td>
                    </div>
                </tr>
                <tr>
                    <div>
                        <td>
                            <label for="done">Done: </label>
                        </td>
                        <td>
                            <c:choose>
                                <c:when test="${task_to_modify.done == true}">
                                    <input id="done" name="done" type="checkbox" checked>
                                </c:when>
                                <c:otherwise>
                                    <input id="done" name="done" type="checkbox">
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </div>
                </tr>
                <tr>
                    <td>
                        <div>
                            <input type="submit" value="Add">
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </c:otherwise>

</c:choose>
</body>
</html>
