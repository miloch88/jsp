<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 03.10.2018
  Time: 19:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Table view</title>
</head>
<body>
<h2>Table:</h2>
<table>
    <c:forEach var="x" begin="1" end="${size_x}">
    <tr>
        <c:forEach var="y" begin="1" end="${size_y}">
        <td>
            <c:out value="${x*y}"/>
        </td>
        </c:forEach>
    </tr>
    </c:forEach>
</table>
</body>
</html>
