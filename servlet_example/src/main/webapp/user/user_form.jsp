<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 03.10.2018
  Time: 20:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User form</title>
</head>
<body>

<h2>User Form:</h2>

<span style="color: red">
<c:out value="${error_message}"/>
</span>

<c:choose>
    <c:when test="${empty user_to_modify}">

        <form action="/user/add" method="post">

            <div>
                <label for="usename">Username:</label>
                <input id="usename" name="username" type="text">
            </div>

            <div>
                <label for="password">Password:</label>
                <input id="password" name="password" type="password">
            </div>

            <div>
                <label for="password-confirm">Password confim:</label>
                <input id="password-confirm" name="password-confirm" type="password">
            </div>

            <div>
                <input type="submit" value="Register">
            </div>
        </form>
    </c:when>

    <c:otherwise>
        <form action="/user/modify" method="post">
            <input type="hidden" name="id" value="<c:out value="${user_to_modify.id}"/>">
            <div>
                <label for="usename">Username:</label>
                <input id="usename" name="username" type="text" value="<c:out value="${user_to_modify.login}"/>">
            </div>

            <div>
                <label for="password">Password:</label>
                <input id="password" name="password" type="password" value="<c:out value="${user_to_modify.password}"/>">
            </div>

            <div>
                <label for="password-confirm">Password confim:</label>
                <input id="password-confirm" name="password-confirm" type="password" value="<c:out value="${user_to_modify.password}"/>">
            </div>

            <div>
                <input type="submit" value="Modify">
            </div>
        </form>

    </c:otherwise>
</c:choose>
</body>
</html>
