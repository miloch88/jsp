package pl.sda.servlet.todos.service;


import pl.sda.servlet.todos.model.AppUser;

import java.util.List;
import java.util.Optional;

public interface UserService {
    public void  addUser(AppUser newUser);
    public List<AppUser> getUserList();
    public void removeWithId(int id);
    public Optional<AppUser> getUserWithId(int id);
    void modify(AppUser appUser);

}
