package pl.sda.servlet.todos.controller;

import pl.sda.servlet.todos.model.TodoTask;
import pl.sda.servlet.todos.service.DependencyManager;
import pl.sda.servlet.todos.service.TaskService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/task/modify")
public class TaskModifyController extends HttpServlet {

    private TaskService taskService;

    public TaskModifyController() {
        taskService = DependencyManager.getInstance().getBean(TaskService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int taskId = Integer.parseInt(req.getParameter("id"));

        Optional<TodoTask> task = taskService.getTaskWithId(taskId);
        if(task.isPresent()){
            req.setAttribute("task_to_modify", task.get());
            req.getRequestDispatcher("/task/task_form.jsp").forward(req, resp);
        }else {
            resp.sendRedirect( "/task/list");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String title = req.getParameter("title");
        String content = req.getParameter("content");
        boolean done = req.getParameter("done") != null ? req.getParameter("done").equals("on") : false;

        if(title.isEmpty() || content.isEmpty()){
            req.setAttribute("error_message", "Title and content cannot be empty!");

            req.getRequestDispatcher("/task/task_form.jsp").forward(req, resp);
            return;
        }

        String id = req.getParameter("id");
        int indentifier = Integer.parseInt(id);
        Optional<TodoTask> task = taskService.getTaskWithId(indentifier);

        if(task.isPresent()){
            TodoTask taskToModify = task.get();

            taskToModify.setTitle(title);
            taskToModify.setContent(content);
            taskToModify.setDone(done);

            taskService.modify(taskToModify);
        }

        resp.sendRedirect(req.getContextPath() + "/task/list");
    }
}
