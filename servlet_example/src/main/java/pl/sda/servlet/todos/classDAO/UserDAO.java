package pl.sda.servlet.todos.classDAO;

import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import pl.sda.servlet.todos.model.AppUser;
import pl.sda.servlet.todos.utility.HibernateUtility;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserDAO {

    public void add(AppUser appUser){
        Transaction transaction = null;
        try(Session session = HibernateUtility.getSf().openSession()){
            transaction = session.beginTransaction();
            session.save(appUser);

            transaction.commit();
        }catch (SessionException se){
            transaction.rollback();
            System.out.println("Error!!!");
        }
    }

    public List<AppUser> getAllUsers(){
        Transaction transaction = null;
        try(Session session = HibernateUtility.getSf().openSession()){
            transaction = session.beginTransaction();
            List<AppUser> list = session.createQuery("FROM AppUser ORDER BY id", AppUser.class).list();
            return list;
        }catch (SessionException se){
            transaction.rollback();
            System.out.println("Error!!!");
        }
        return new ArrayList<>();
    }

    public void removeWithId(int id){
        Optional<AppUser> optUser = findUserWithId(id);

        if(optUser.isPresent()){
            AppUser user = optUser.get();
            Transaction transaction = null;
            try(Session session = HibernateUtility.getSf().openSession()){
                transaction = session.beginTransaction();
                session.remove(user);

                transaction.commit();
            }catch (SessionException se){
                transaction.rollback();
                System.out.println("Error!!!");
            }
        }
    }

    public Optional<AppUser> findUserWithId(int id) {
        Transaction transaction = null;
        try(Session session = HibernateUtility.getSf().openSession()){
            transaction = session.beginTransaction();

            Query<AppUser> query =session.createQuery("FROM AppUser WHERE id= :id", AppUser.class);
            query.setParameter("id", id);

            AppUser result = query.uniqueResult();

            return Optional.of(result);
        }catch (SessionException se){
            transaction.rollback();
            System.out.println("Error!!!");
        }
        return Optional.empty();
    }

    public void modify(AppUser appUser) {
        Transaction transaction = null;
        try(Session session = HibernateUtility.getSf().openSession()){
            transaction = session.beginTransaction();

            session.saveOrUpdate(appUser);

            transaction.commit();
        }catch (SessionException se){
            transaction.rollback();
            System.out.println("Error!!!");
        }
    }
}
