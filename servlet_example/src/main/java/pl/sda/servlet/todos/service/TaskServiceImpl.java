package pl.sda.servlet.todos.service;

import pl.sda.servlet.todos.classDAO.TaskDAO;
import pl.sda.servlet.todos.model.TodoTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TaskServiceImpl implements TaskService{

    private TaskDAO taskDAO;

    public TaskServiceImpl() {
        taskDAO = new TaskDAO();
    }

    @Override
    public void addTask(TodoTask todoTask) {
        taskDAO.add(todoTask);
    }

    @Override
    public List<TodoTask> getTodoTaskList() {
        return taskDAO.getAllTasks();
    }

    @Override
    public void removeTaskWithId(int id) {
        taskDAO.removeWithId(id);
    }

    @Override
    public Optional<TodoTask> getTaskWithId(int id) {
        return taskDAO.findTaskWithId(id);
    }

    @Override
    public void modify(TodoTask todoTask) {
        taskDAO.modify(todoTask);

    }
}
