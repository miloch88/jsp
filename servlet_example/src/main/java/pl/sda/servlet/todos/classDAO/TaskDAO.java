package pl.sda.servlet.todos.classDAO;

import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import pl.sda.servlet.todos.model.AppUser;
import pl.sda.servlet.todos.model.TodoTask;
import pl.sda.servlet.todos.utility.HibernateUtility;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TaskDAO {

    public void add(TodoTask todoTask) {
        Transaction transaction = null;
        try (Session session = HibernateUtility.getSf().openSession()) {
            transaction = session.beginTransaction();
            session.save(todoTask);

            transaction.commit();
        } catch (SessionException se) {
            transaction.rollback();
            System.out.println("Error!!!");
        }
    }

    public List<TodoTask> getAllTasks() {
        Transaction transaction = null;
        try (Session session = HibernateUtility.getSf().openSession()) {
            transaction = session.beginTransaction();
            List<TodoTask> list = session.createQuery("FROM TodoTask ORDER BY id", TodoTask.class).list();
            return list;
        } catch (SessionException se) {
            transaction.rollback();
            System.out.println("Error!!!");
        }
        return new ArrayList<>();
    }

    public void removeWithId(int id) {
        Optional<TodoTask> optUser = findTaskWithId(id);

        if (optUser.isPresent()) {
            TodoTask user = optUser.get();
            Transaction transaction = null;
            try (Session session = HibernateUtility.getSf().openSession()) {
                transaction = session.beginTransaction();
                session.remove(user);

                transaction.commit();
            } catch (SessionException se) {
                transaction.rollback();
                System.out.println("Error!!!");
            }
        }
    }

    public Optional<TodoTask> findTaskWithId(int id) {
        Transaction transaction = null;
        try (Session session = HibernateUtility.getSf().openSession()) {
            transaction = session.beginTransaction();

            Query<TodoTask> query = session.createQuery("FROM TodoTask WHERE id= :id", TodoTask.class);
            query.setParameter("id", id);

            TodoTask result = query.uniqueResult();

            return Optional.of(result);
        } catch (SessionException se) {
            transaction.rollback();
            System.out.println("Error!!!");
        }
        return Optional.empty();
    }

    public void modify(TodoTask todoTask) {
        Transaction transaction = null;
        try (Session session = HibernateUtility.getSf().openSession()) {
            transaction = session.beginTransaction();

            session.saveOrUpdate(todoTask);

            transaction.commit();
        } catch (SessionException se) {
            transaction.rollback();
            System.out.println("Error!!!");
        }
    }
}

