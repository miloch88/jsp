package pl.sda.servlet.todos.service;

import java.util.HashMap;
import java.util.Map;
/*
DependencyManager to klasa przechowywyująca nasze beany. Przechowuje w mapie wszystkie obiekty gdzie
kluczem jest typ obietku a wartością jego instancja. Dzięki temu, że jest to singleton, mamy możliwość
pobrania bean'o z dowolnego miejsca w kodzie
 */
public class DependencyManager {
    //Singleton
    private static DependencyManager ourInstance = new DependencyManager();

    public static DependencyManager getInstance() {
        return ourInstance;
    }

    //konstrukotr
    private DependencyManager() {
        //w konstruktorze musimy dodać wszystkie beam'y które manager będzie przrechowywać
        registerBean(UserService.class, new UserServiceImpl());
        registerBean(TaskService.class, new TaskServiceImpl());
    }

    private Map<Class<?>, Object> beanMap = new HashMap<>();

    //DependencyManager.getInstance().getBean(UserService.class)
    public <T> T getBean(Class<T> className){
        return (T) beanMap.get(className);
    }

    private void registerBean(Class<?> className, Object o){
        beanMap.put(className, o);
    }
}
