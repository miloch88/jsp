package pl.sda.servlet.todos.service;

import pl.sda.servlet.todos.model.TodoTask;

import java.util.List;
import java.util.Optional;

public interface TaskService {

    void addTask(TodoTask todoTask);
    List<TodoTask> getTodoTaskList();
    void removeTaskWithId(int id);
    Optional<TodoTask> getTaskWithId(int id);
    void modify(TodoTask todoTask);
}
