package pl.sda.servlet.todos.service;

import pl.sda.servlet.todos.classDAO.UserDAO;
import pl.sda.servlet.todos.model.AppUser;

import javax.ejb.Singleton;
import javax.enterprise.inject.Model;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserServiceImpl implements UserService {

    private UserDAO userDAO;

    public UserServiceImpl() {
        userDAO = new UserDAO();
    }

    public void  addUser(AppUser newUser){
        userDAO.add(newUser);
    }

    public List<AppUser> getUserList() {
        return userDAO.getAllUsers();
    }

    @Override
    public void removeWithId(int id) {
        userDAO.removeWithId(id);
    }

    @Override
    public Optional<AppUser> getUserWithId(int id) {
        return userDAO.findUserWithId(id);
    }

    public void modify(AppUser appUser){
        userDAO.modify(appUser);
    }
}
