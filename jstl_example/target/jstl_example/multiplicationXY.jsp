<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 02.10.2018
  Time: 10:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Multiplication XY </title>
</head>
<body>

<form action="multiplicationXY.jsp" method="get">


<input name="x" id="x" type="number" min="1">
<input name="y" id="y" type="number" min="1">
<input type="submit" value="Calculate">

</form>

<c:choose>
    <c:when test="${param.x >1 && param.y >1}">

<table>

    <c:forEach var="i" begin="1" end="${param.x}">
        <tr>
            <c:forEach var="j" begin="1" end="${param.y}">
                <td>
                    <c:out value="${i*j}"/>
                </td>
            </c:forEach>
        </tr>
    </c:forEach>

</table>
    </c:when>

    <c:otherwise>
        <h2>Brak danych!</h2>
    </c:otherwise>


</c:choose>
</body>
</html>
