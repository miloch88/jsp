<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.io.FileWriter" %>
<%@ page import="java.io.FileNotFoundException" %><%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 03.10.2018
  Time: 18:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Save to file</title>
</head>
<body>
<%@include file="header.jsp" %>
<%
    List<Student> studentList = getStudentList(session);

    try (PrintWriter writer = new PrintWriter("students.txt")) {
        for (Student s : studentList) {
            writer.println(s.writeToFile());
        }
    } catch (FileNotFoundException e) {
        out.print("Error!");
    }

    response.sendRedirect("student_list.jsp");
%>

</body>
</html>
