<%@ page import="java.util.Iterator" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 02.10.2018
  Time: 19:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student List</title>
</head>
<body>
<%@include file="header.jsp" %>


<h2>Student List</h2>

<table style="width: 100%; border: solid 1px #000000">

    <thead align="left">

    <th> Id</th>
    <th> Name</th>
    <th> Surname</th>
    <th> Index</th>
    <th> Birthday</th>
    <th></th>
    <th></th>

    </thead>

    <c:forEach var="s" items="${sessionScope.student_list}">

        <tr>
            <td>
                <c:out value="${s.id}"/>
            </td>
            <td>
                <c:out value="${s.name}"></c:out>
            </td>
            <td>
                <c:out value="${s.surname}"></c:out>
            </td>
            <td>
                <c:out value="${s.index}"></c:out>
            </td>
            <td>
                <c:out value="${s.birthdate}"></c:out>
            </td>

            <td>
                <a href="remove_student.jsp?id=<c:out value="${s.id}"/>">remove</a>
            </td>

            <td>
                <a href="student_form.jsp?id=<c:out value="${s.id}"/>">modify</a>
            </td>

        </tr>
    </c:forEach>

</table>


<div>
    <table>
        <tr>
            <td>
                <form action="student_form.jsp" method="get">
                    <input type="submit" value="Add">
                </form>
            </td>
            <td>
                <form action="save_toFile.jsp">
                    <input type="submit" value="Save">
                </form>
            </td>
            <td>
                <form action="load_from_file.jsp">
                    <input type="submit" value="Load">
                </form>
            </td>
        </tr>
    </table>
</div>

</body>
</html>
