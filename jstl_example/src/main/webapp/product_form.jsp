<%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 02.10.2018
  Time: 18:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Product form</title>
</head>
<body>
<%--W formularzu powunny znaleźć się się pola opisujące produkt--%>
<h2>Product Form</h2>

<form action="product_details.jsp" method="get">

    <div>
        <label for="name">Name: </label>
        <input type="text" name="name" id="name">
    </div>
    <div>
        <label for="price">Price: </label>
        <input type="number" name="price" id="price" step="0.01">
    </div>
    <div>
        <label for="expireDate">Expire Date: </label>
        <input type="date" name="expireDate" id="expireDate">
    </div>
    <div>
        <label for="amount">Amount: </label>
        <input type="number" name="amount" id="amount">
    </div>

    <div>
        <tr>
            <form action="product_form.jsp" method="get">
                <input type="submit" value="Add">
            </form>
        </tr>
    </div>

</form>
</body>
</html>
