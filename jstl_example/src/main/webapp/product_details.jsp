<%@ page import="jstl.Product" %>
<%@ page import="java.time.LocalDate" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 02.10.2018
  Time: 18:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Product details</title>
</head>
<body>
<%! int licznik = 0; %>
<h2>Product details</h2>

    <%
        if(request.getParameter("name")!= null &&
                request.getParameter("amount") != null &&
                request.getParameter("price") != null &&
                request.getParameter("expireDate") != null
        ) {
            Product product = new Product();
            product.setName(request.getParameter("name"));
            product.setPrice(Double.parseDouble(request.getParameter("price")));
            product.setExpireDate(LocalDate.parse(request.getParameter("expireDate")));
            product.setAmount(Integer.parseInt(request.getParameter("amount")));
            product.setId(licznik++);

            pageContext.setAttribute("product", product);
        }
    %>

    <c:choose>
        <c:when test="${not empty product}">

            <table style="width: 100%; border: solid 1px #000000">

                <thead align="left">

                <th> Id</th>
                <th> Name</th>
                <th> Price</th>
                <th> Expire Date</th>
                <th> Amount</th>

                </thead>

                <tr>
                    <td>
                        <c:out value="${product.id}"></c:out>
                    </td>
                    <td>
                        <c:out value="${product.name}"></c:out>
                    </td>
                    <td>
                        <c:out value="${product.price}"></c:out>
                    </td>
                    <td>
                        <c:out value="${product.expireDate}"></c:out>
                    </td>
                    <td>
                        <c:out value="${product.amount}"></c:out>
                    </td>

                </tr>

            </table>

        </c:when>
        <c:otherwise>
            <h3> Product details missing!</h3>
        </c:otherwise>
    </c:choose>

<div>
    <tr>
        <form action="product_form.jsp" method="get">
            <input type="submit" value="Add">
        </form>
    </tr>
</div>



</body>
</html>
