<%@ page import="jstl.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<table>

    <tr>
        <td><a href="student_list.jsp">Student List</a></td>
        <td><a href="student_form.jsp">Student Form</a></td>
    </tr>

    <%!
        public List<Student> getStudentList(HttpSession session){
            List<Student> studentList;
            if(session.getAttribute("student_list") != null){
                studentList = (List<Student>) session.getAttribute("student_list");
            }else {
                studentList = new ArrayList<>();
            }
            return studentList;
        }

    %>
</table>