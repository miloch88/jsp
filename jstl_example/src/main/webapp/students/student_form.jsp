<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 02.10.2018
  Time: 19:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student Form</title>
</head>
<body>
<%@include file="header.jsp" %>

<%
    List<Student> studentList = getStudentList(session);

    if (request.getParameter("id") != null) {

        int id = Integer.parseInt(request.getParameter("id"));

        for (Student st : studentList) {
            if (st.getId() == id) {
                pageContext.setAttribute("student", st);
                break;
            }
        }
    }

%>

<h2>Add Student</h2>

<c:choose>
    <c:when test="${not empty student}">

        <form action="student_modify.jsp" method="get">
                <%--Żeby wiedzieć który jest edytowany--%>
            <input type="hidden" name="id" value="<c:out value="${student.id}"/>">
            <div>
                <label for="name">Name:</label>
                <input id="name" name="name" type="text" value="<c:out value="${student.name}"/>">
            </div>
            <div>
                <label for="surname">Surname:</label>
                <input id="surname" name="surname" type="text" value="<c:out value="${student.surname}"/>">
            </div>
            <div>
                <label for="index">Index:</label>
                <input id="index" name="index" type="text" value="<c:out value="${student.index}"/>">
            </div>
            <div>
                <label for="birthdate">Birthday:</label>
                <input id="birthdate" name="birthdate" type="date" value="<c:out value="${student.birthdate}"/>">
            </div>

            <div>
                <tr>
                    <input type="submit" value="Modify">
                </tr>
            </div>
        </form>
    </c:when>
    <c:otherwise>

        <form action="student_add.jsp" method="get">

            <div>
                <label for="name">Name:</label>
                <input id="name" name="name" type="text">
            </div>
            <div>
                <label for="surname">Surname:</label>
                <input id="surname" name="surname" type="text">
            </div>
            <div>
                <label for="index">Index:</label>
                <input id="index" name="index" type="text">
            </div>
            <div>
                <label for="birthdate">Birthday:</label>
                <input id="birthdate" name="birthdate" type="date">
            </div>

            <div>
                <tr>
                    <input type="submit" value="Add">
                </tr>
            </div>
        </form>
    </c:otherwise>
</c:choose>
</body>
</html>
