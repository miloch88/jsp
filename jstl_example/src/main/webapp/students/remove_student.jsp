<%@ page import="java.util.Iterator" %><%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 02.10.2018
  Time: 20:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Remove Student</title>
</head>
<body>

<%@ include file="header.jsp"%>

<%
    List<Student> stuentList = getStudentList(session);

    String removedStringId = request.getParameter("id");
    int id = Integer.parseInt(removedStringId);

    Iterator<Student> it = stuentList.iterator();
    while (it.hasNext()){
        Student student = it.next();
        if(student.getId() == id){
            it.remove();
            break;
        }
    }

    session.setAttribute("student_list", stuentList);

    response.sendRedirect("student_list.jsp");
%>
</body>
</html>
