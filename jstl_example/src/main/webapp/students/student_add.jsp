<%@ page import="jstl.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.time.LocalDate" %><%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 02.10.2018
  Time: 19:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student Add</title>
</head>
<body>
<%! int licznik = 0; %>

<%
    List<Student> studentList;
    if (session.getAttribute("student_list") != null) {
        studentList = (List<Student>) session.getAttribute("student_list");
    } else {
        studentList = new ArrayList<>();
    }

    Student student = new Student();
    student.setId(licznik++);
    student.setName(request.getParameter("name"));
    student.setSurname(request.getParameter("surname"));
    student.setIndex(request.getParameter("index"));
    student.setBirthdate(LocalDate.parse(request.getParameter("birthdate")) );

    studentList.add(student);

    session.setAttribute("student_list", studentList);
    response.sendRedirect("student_list.jsp");


%>


</body>
</html>
