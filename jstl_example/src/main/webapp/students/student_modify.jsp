<%@ page import="jstl.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="java.time.LocalDate" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 03.10.2018
  Time: 18:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student Modify</title>
</head>
<body>

<%@include file="header.jsp" %>

<%
    List<Student> studentList = getStudentList(session);


    
    int id = Integer.parseInt(request.getParameter("id"));

    Student searched = null;
    for (Student st : studentList) {
        if (st.getId() == id) {
            searched = st;
            break;
        }
    }


    searched.setName(request.getParameter("name"));
    searched.setSurname(request.getParameter("surname"));
    searched.setIndex(request.getParameter("index"));
    searched.setBirthdate(LocalDate.parse(request.getParameter("birthdate")));

    session.setAttribute("student_list", studentList);

    response.sendRedirect("student_list.jsp");
%>


</body>
</html>
