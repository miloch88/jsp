<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 30.09.2018
  Time: 17:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Multiplication Table</title>
</head>
<body>



    <table>

        <c:forEach var="i" begin="1" end="50">
           <tr>
               <c:forEach var="j" begin="1" end="50">
                   <td>
                        <c:out value="${i*j}"/>
                   </td>
                </c:forEach>
           </tr>
        </c:forEach>

    </table>

</body>
</html>
