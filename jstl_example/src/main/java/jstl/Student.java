package jstl;

import java.time.LocalDate;

public class Student {

    private int id;
    private  String name;
    private String surname;
    private String index;
    private LocalDate birthdate;

    public Student() {
    }

    public Student(int id, String name, String surname, String index, LocalDate birthdate) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.index = index;
        this.birthdate = birthdate;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public String writeToFile() {
        StringBuilder builder = new StringBuilder();

        return builder.append(id).append(";")
                .append(name).append(";")
                .append(surname).append(";")
                .append(index).append(";")
                .append(birthdate).append(";").toString();
    }
}
