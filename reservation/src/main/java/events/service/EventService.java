package events.service;

import events.dao.EventDao;
import events.model.AppUser;
import events.model.Event;
import events.model.dto.EventDto;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@LocalBean
@Singleton
public class EventService {

    @EJB
    private EventDao eventDao;
    @EJB
    private UserService userService;

    @PostConstruct
    public void init(){
        System.out.println("Event was initialized ");
    }

    public void addEvent(Event event){
        eventDao.addEvent(event);
    }

    public List<Event> getEventList(){
        return eventDao.getAllEvents();
    }

    public Optional<Event> addNewEvent(EventDto eventDto) {
        Event event = new Event();
        event.setName(eventDto.getName());
        event.setTime(LocalDateTime.parse(eventDto.getTime(), DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        event.setLength(eventDto.getLength());
        event.setDescription(eventDto.getDescription());

        Long owner_id = Long.parseLong(eventDto.getOwnerId());
        Optional<AppUser> appUserOptional = userService.findUserById(owner_id);
        if(appUserOptional.isPresent()){
            event.setOwner(appUserOptional.get());

            eventDao.addEvent(event);

            return Optional.of(event);
        }

        return null;
    }
}
