package events.service;

import events.dao.UserDao;
import events.model.AppUser;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import java.util.Optional;

//@EJB + @Singleton w UserService zamiast MaangerDependency
@LocalBean
@Singleton
public class UserService {

    @EJB
    private UserDao userDao;

    @PostConstruct
    public void init(){
        System.out.println("Initialized!");
    }

    public boolean ckeckUsernameNotExists(String username) {
        Optional<AppUser> appUserOptional = userDao.getUserWithUsername(username);
        if(appUserOptional.isPresent()){
            return false;
        }
        return true;
    }

    public boolean ckeckEmailNotExists(String email) {
        Optional<AppUser> appUserOptional = userDao.getUserWithEmail(email);
        if(appUserOptional.isPresent()){
        return false;
        }
        return true;
    }

    public void register(AppUser appUser) {
        userDao.add(appUser);
    }

    public Optional<Long> login(String login, String password) {
        Optional<AppUser> appUserOptional = userDao.getUserWithLoginAndPassword(login, password);
        if(appUserOptional.isPresent()){
            return Optional.of(appUserOptional.get().getId());
        }
        return Optional.empty();
    }

        /////////////////////////////////////////
    public AppUser getUser(long id){
        Optional<AppUser> appUserOptional = userDao.findUserWithId(id);{
            AppUser appUser = null;
            if(appUserOptional.isPresent()){
                return appUser = appUserOptional.get();
            }
            return null;
        }
    }

    public Optional<AppUser> findUserById(Long loggedIserId) {
        return userDao.findUserWithId(loggedIserId);
    }
}
