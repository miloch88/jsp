package events.servlets;

import events.model.AppUser;
import events.model.Event;
import events.service.EventService;
import events.service.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@WebServlet("/event/add")
public class EventAddServlet extends HttpServlet {

    @EJB
    private EventService eventService;
    @EJB
    private UserService userService;


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if(session == null || session.getAttribute("logged_id") == null){
            resp.sendRedirect(req.getContextPath() + "/");
        return;}
        req.getRequestDispatcher("/event/event_add.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

//        String name = req.getParameter("name");
//        String description = req.getParameter("description");
//        String length = req.getParameter("length");
//        String time = req.getParameter("time");
//        String owner_id = req.getParameter("owner_id");
//
//        System.out.println(name + ";" + length + ";" + description + ";" + time + ";" + owner_id);

        Event event = new Event();
        event.setName(req.getParameter("name"));
        event.setDescription(req.getParameter("description"));
        event.setLength(Integer.parseInt(req.getParameter("length")));
        event.setTime(LocalDateTime.parse(req.getParameter("time"), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")));

        ///////////////////////////////////////////
        AppUser appUser = userService.getUser(Long.parseLong(req.getParameter("owner_id")));
        appUser.addEvent(event);

        eventService.addEvent(event);

        resp.sendRedirect(req.getContextPath() + "/event/list");
    }
}
