package events.dao;

import events.model.AppUser;
import events.model.Event;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.swing.text.html.ListView;
import java.util.ArrayList;
import java.util.List;

@LocalBean
@Singleton
public class EventDao {

    public void addEvent(Event event){
        Transaction transaction = null;
        try(Session session = HibernateUtility.getSf().openSession()){
            transaction = session.beginTransaction();
            session.save(event);

            transaction.commit();
        }catch (SessionException se){
            transaction.rollback();
            System.out.println("Error!!!");
        }
    }

    public List<Event> getAllEvents(){
        Transaction transaction = null;
        try(Session session = HibernateUtility.getSf().openSession()){
            transaction = session.beginTransaction();
            List<Event> events = session.createQuery("FROM Event", Event.class).list();

            return events;
        }catch (SessionException se){
            transaction.rollback();
            System.out.println("Error!!!");
        }
        return new ArrayList<>();
    }
}
