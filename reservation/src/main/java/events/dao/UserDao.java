package events.dao;

import events.model.AppUser;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@LocalBean
@Singleton
public class UserDao {

    public void add(AppUser appUser){
        Transaction transaction = null;
        try(Session session = HibernateUtility.getSf().openSession()){
            transaction = session.beginTransaction();
            session.save(appUser);

            transaction.commit();
        }catch (SessionException se){
            transaction.rollback();
            System.out.println("Error!!!");
        }
    }

    public List<AppUser> getAllUsers(){
        Transaction transaction = null;
        try(Session session = HibernateUtility.getSf().openSession()){
            transaction = session.beginTransaction();
            List<AppUser> list = session.createQuery("FROM AppUser ORDER BY id", AppUser.class).list();
            return list;
        }catch (SessionException se){
            transaction.rollback();
            System.out.println("Error!!!");
        }
        return new ArrayList<>();
    }

    public void removeWithId(int id){
        Optional<AppUser> optUser = findUserWithId(id);

        if(optUser.isPresent()){
            AppUser user = optUser.get();
            Transaction transaction = null;
            try(Session session = HibernateUtility.getSf().openSession()){
                transaction = session.beginTransaction();
                session.remove(user);

                transaction.commit();
            }catch (SessionException se){
                transaction.rollback();
                System.out.println("Error!!!");
            }
        }
    }

    public Optional<AppUser> findUserWithId(long id) {
        Transaction transaction = null;
        try(Session session = HibernateUtility.getSf().openSession()){
            transaction = session.beginTransaction();

            Query<AppUser> query =session.createQuery("FROM AppUser WHERE id= :id", AppUser.class);
            query.setParameter("id", id);

            AppUser result = query.uniqueResult();

            return Optional.of(result);
        }catch (SessionException se){
            transaction.rollback();
            System.out.println("Error!!!");
        }
        return Optional.empty();
    }

    public void modify(AppUser appUser) {
        Transaction transaction = null;
        try(Session session = HibernateUtility.getSf().openSession()){
            transaction = session.beginTransaction();

            session.saveOrUpdate(appUser);

            transaction.commit();
        }catch (SessionException se){
            transaction.rollback();
            System.out.println("Error!!!");
        }
    }

    public Optional<AppUser> getUserWithLoginAndPassword(String login, String password) {
        Transaction transaction = null;
        try(Session session = HibernateUtility.getSf().openSession()){
            transaction = session.beginTransaction();

            Query<AppUser> query =session.createQuery("FROM AppUser WHERE username= :username and password= :password", AppUser.class);

            query.setParameter("username", login);
            query.setParameter("password", password);

            AppUser result = query.uniqueResult();

            return Optional.ofNullable(result);
        }catch (SessionException se){
            transaction.rollback();
            System.out.println("Error!!!");
        }
        return Optional.empty();
    }

    public Optional<AppUser> getUserWithEmail(String email) {
        Transaction transaction = null;
        try(Session session = HibernateUtility.getSf().openSession()){
            transaction = session.beginTransaction();

            Query<AppUser> query =session.createQuery("FROM AppUser WHERE email= :email", AppUser.class);

            query.setParameter("email", email);

            AppUser result = query.uniqueResult();

            return Optional.ofNullable(result);
        }catch (SessionException se){
            transaction.rollback();
            System.out.println("Error!!!");
        }
        return Optional.empty();
    }

    public Optional<AppUser> getUserWithUsername(String username) {
        Transaction transaction = null;
        try(Session session = HibernateUtility.getSf().openSession()){
            transaction = session.beginTransaction();

            Query<AppUser> query =session.createQuery("FROM AppUser WHERE username= :username", AppUser.class);

            query.setParameter("username", username);

            AppUser result = query.uniqueResult();

            return Optional.ofNullable(result);
        }catch (SessionException se){
            transaction.rollback();
            System.out.println("Error!!!");
        }
        return Optional.empty();
    }

}
