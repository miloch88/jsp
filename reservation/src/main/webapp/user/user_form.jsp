<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 08.10.2018
  Time: 17:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Register form</title>
</head>
<body>
<%@include file="/header.jsp"%>
<h2>User Form:</h2>

<span style="color: red">
<c:out value="${error_message}"/>
</span>


<form action="/user/register" method="post">

    <div>
        <label for="email">Email:</label>
        <input id="email" name="email" type="email">
    </div>

    <div>
        <label for="usename">Username:</label>
        <input id="usename" name="username" type="text">
    </div>

    <div>
        <label for="password">Password:</label>
        <input id="password" name="password" type="password">
    </div>

    <div>
        <label for="password-confirm">Password confim:</label>
        <input id="password-confirm" name="password-confirm" type="password">
    </div>

    <div>
        <input type="submit" value="Register">
    </div>
</form>


</body>
</html>
