<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 09.10.2018
  Time: 18:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Event List</title>
</head>
<body>
<%@include file="/header.jsp"%>
<h2>Your event list: </h2>
<table style="width: 50%">


    <thead align="left">
    <th>Id</th>
    <th>Name</th>
    <th>Description</th>
    <th>Time</th>
    <th>Length</th>
    <th>Remove</th>
    <th>Modify</th>
    </thead>

    <c:forEach var="event" items="${eventList}">
        <tr>

        <td><c:out value="${event.id}"></c:out></td>
        <td><c:out value="${event.name}"></c:out></td>
        <td><c:out value="${event.description}"></c:out></td>
        <td><c:out value="${event.time}"></c:out></td>
        <td><c:out value="${event.length}"></c:out></td>
        <td>Remove</td>
        <td>Modify</td>
        </tr>
    </c:forEach>


</table>

</body>
</html>
