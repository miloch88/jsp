<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 08.10.2018
  Time: 19:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<%@include file="header.jsp" %>
<h2>Login form:</h2>

<form action="/login" method="post">

    <div>
        <label for="usename">Username:</label>
        <input id="usename" name="username" type="text">
    </div>

    <div>
        <label for="password">Password:</label>
        <input id="password" name="password" type="password">
    </div>

    <div>
        <input type="submit" value="Login">
    </div>
</form>
</body>
</html>
