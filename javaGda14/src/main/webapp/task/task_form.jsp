<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 10/5/18
  Time: 5:37 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Task form</title>
</head>
<body>
<c:choose>
    <%--Sprawdzenie czy mamy task do zmodyfikowania--%>
    <c:when test="${not empty task_to_modify}">
        <%--Ponieważ mamy użycie method = post, w klasie TaskModifyController
            wywoła się metoda doPost().

            gdybyśmy użyli method get, wywołałaby się metoda doGet...
        --%>
        <form action="/task/modify" method="post">
            <input type="hidden" name="id" value="<c:out value="${task_to_modify.id}"/>">
            <div>
                <label for="title">Title:</label>
                <input id="title" name="title" type="text" value="<c:out value="${task_to_modify.title}"/>">
            </div>
            <div>
                <label for="content">Content:</label>
                <input id="content" name="content" type="text"
                       value="<c:out value="${task_to_modify.content}"/>">
            </div>
            <div>
                <label for="done">Is done:</label>
                <c:choose>
                    <c:when test="${task_to_modify.done == 'true'}">
                        <input id="done" name="done" type="checkbox" checked>
                    </c:when>
                    <c:otherwise>
                        <input id="done" name="done" type="checkbox">
                    </c:otherwise>
                </c:choose>
            </div>
            <%--Wszystkie pola formularza zostaną przesłane jako parametry
                wywołania (request.getParameters). Nazwa parametru bierze się z
                pola name, wartość z treści wypełnionej w polu.
            --%>
            <input type="submit" value="Modify">
        </form>
    </c:when>
    <%--jeśli nie mamy danych do zmodyfikowania to powinien się wyświetlić formularz
        dodawania rekrodu zamiast modyfikacji
        --%>
    <c:otherwise>
        <form action="/task/add" method="post">
            <div>
                <label for="title">Title:</label>
                <input id="title" name="title" type="text">
            </div>
            <div>
                <label for="content">Content:</label>
                <input id="content" name="content" type="text">
            </div>
            <div>
                <label for="done">Is Done:</label>
                <input id="done" name="done" type="checkbox">
            </div>
            <input type="submit" value="Add">
        </form>
    </c:otherwise>
</c:choose>
</body>
</html>
