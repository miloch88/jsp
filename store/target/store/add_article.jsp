<%@ page import="store.Article" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %><%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 26.09.2018
  Time: 10:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Article</title>
</head>
<body>

<%
    List<Article> articles;
    if(session.getAttribute("article_list") != null){
        articles = (List<Article>) session.getAttribute("article_list");
    }else{
        articles = new ArrayList<>();
    }

    //Pobieramy z GET id
    String articleID = request.getParameter("article_id");

    Article searched = null;

    if(articleID != null){
        int modifiedID = Integer.parseInt(articleID);
        Iterator<Article> it = articles.iterator();
        while (it.hasNext()){
            Article article = it.next();
            if(article.getId() == modifiedID){
                searched = article;
                break;
            }
        }
    }
%>


    <h1 align="center"> Add Article </h1>
    <hr>
<form action="<%= searched == null ? "add_article_to_list.jsp" : "modify_article.jsp"%>" method="get">
    <input type="hidden" hidden value="<%= searched == null ? "" : searched.getId() %>" name="modified_id">
    <table>

        <tr>
        <div>
            <td><label for="name"> Name: </label></td>
            <td><input id="name" name="name" type="text"
                       value="<%= searched == null ? "" : searched.getName()%>"></td>
        </div>
        </tr>

        <tr>
        <div>
            <td> <label for="quantity"> Quantity: </label> </td>
            <td> <input id="quantity" name="quantity" type="number"
                        value="<%= searched == null ? "" : searched.getQuantity()%>" > </td>
        </div>
        </tr>

        <tr>
            <div>
                <td><label for="validity"> Validity: </label></td>
                <td><input id="validity" name="validity" type="date"
                           value="<%= searched == null ? "" : "disabled"%>"></td>
            </div>
        </tr>

        <%--<tr>--%>
            <%--<div id="type">--%>
                <%--<td><label for="type"> Type: </label></td>--%>
                <%--<td>--%>
                    <%--<select>--%>
                        <%--<option value="FOOD">Food</option>--%>
                        <%--<option value="ELECTRONIC">Electronic</option>--%>
                        <%--<option value="ARTIFICAL">Artifical</option>--%>
                        <%--<option value="DOMESTIC">Domestic</option>--%>
                    <%--</select>--%>
                <%--</td>--%>
            <%--</div>--%>
        <%--</tr>--%>



    </table>
        <p> </p>
        <tr>
            <input type="submit" value="Add">
        </tr>

</form>
</body>
</html>
