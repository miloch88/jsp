<%@ page import="store.Article" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.io.BufferedReader" %>
<%@ page import="java.io.FileReader" %>
<%@ page import="java.io.FileNotFoundException" %><%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 01.10.2018
  Time: 17:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Load from File</title>
</head>
<body>

<%
    List<Article> articles = new ArrayList<>();

    try (BufferedReader br = new BufferedReader(new FileReader("articles.txt"))){
        String linia;
        while ((linia = br.readLine()) != null){
            Article article = new Article();
            article.loadFromFile(linia);

            articles.add(article);
        }
    }catch (FileNotFoundException e){
        out.print("Error!");
    }

    session.setAttribute("article_list", articles);

    response.sendRedirect("articles_list.jsp");
%>

</body>
</html>
