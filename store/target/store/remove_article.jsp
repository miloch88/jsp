<%@ page import="store.Article" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %><%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 28.09.2018
  Time: 12:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Remove Article</title>
</head>
<body>

    <%
        List<Article> articles;
        //sprawdzamy czy w sesji znaduję się nasza lista
        if(session.getAttribute("article_list") != null){
            //wyciągamy listę userów z sesji jeśli tam jest
            articles = (List<Article>) session.getAttribute("article_list");
        }else {
            //jeśli nie ma tworzymy nową listę
            articles = new ArrayList<>();
        }

        String removedStingID = request.getParameter("article_id");
        int removedID = Integer.parseInt(removedStingID);

        //Tworzymy iterator listy articles
        Iterator<Article> it = articles.iterator();
        //dopóki mam następne elementy
        while (it.hasNext()){
            //przechodzę do nstępneo elemtnu
            Article article = it.next();

            //jeśli obecny element to ten,  który mam usunąć
            if(article.getId() == removedID){
                //usuwam element
                it.remove();
                //i kończę obieg pętli
                break;
            }
        }

        //umieszczam listę z powrotem w sesji
        session.setAttribute("article_list", articles);

        response.sendRedirect("articles_list.jsp");

    %>

</body>
</html>
