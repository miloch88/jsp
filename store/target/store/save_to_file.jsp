<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.io.FileNotFoundException" %><%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 01.10.2018
  Time: 15:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Save to file</title>
</head>
<body>

<%@include file="header.jsp"%>

<%

    List<Article> articles = getList(session);

    try(PrintWriter writer = new PrintWriter("articles.txt")){

        for (Article a: articles){
            writer.println(a.writeToFile());
        }
    }catch (FileNotFoundException e){
        out.print("Error!");
    }

    response.sendRedirect("articles_list.jsp");
%>

</body>
</html>
