<%@ page import="store.Article" %>
<%@ page import="store.TYPE" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="java.util.*" %>
<%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 26.09.2018
  Time: 10:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Articles List</title>
</head>
<body>

<h1 align="center"> Articles List </h1>
<hr>

<%--Dodaje linki na początku strony--%>
<%--<jsp:include page="header.jsp"></jsp:include>--%>

<%--Tworzymy linki do modyfikacji i usuwania --%>

<%@include file="header.jsp"%>
<%!
    public String modifyLink(int id) {
        StringBuilder builder = new StringBuilder();

        builder.append("<a href=\"add_article.jsp?article_id=" + id);
        builder.append("\">modify</a>");
        return builder.toString();
    }

    public String removeLink(int id) {
        StringBuilder builder = new StringBuilder();

        builder.append("<a href=\"remove_article.jsp?article_id=" + id);
        builder.append("\">remove</a>");
        return builder.toString();
    }
%>

<%
    List<Article> articles = getList(session);

//    if (session.getAttribute("article_list") != null) {
//        articles = (List<Article>) session.getAttribute("article_list");
//    } else {
//        articles = new ArrayList<>();
//    }
//
//    Collections.sort(articles, new Comparator<Article>() {
//        @Override
//        public int compare(Article article1, Article article2) {
//            if (article1.getId() > article2.getId()) {
//                return 1;
//            } else if (article1.getId() < article2.getId()) {
//                return -1;
//            }
//            return 0;
//        }
//    });


%>

<table style="width: 100%; border: solid 1px #000000">

    <thead align="left">
    <th> Id</th>
    <th> Name</th>
    <th> Quantity</th>
    <th> Type</th>
    <th> Validity</th>
    <th> Add Date</th>
    <th></th>
    <th></th>
    </thead>

    <%
        for (Article a : articles) {
            out.print("<tr>");

            out.print("<td>");
            out.print(a.getId());
            out.print("</td>");

            out.print("<td>");
            out.print(a.getName());
            out.print("</td>");

            out.print("<td>");
            out.print(a.getQuantity());
            out.print("</td>");

            //TYPE!!!!!!!!!!!!!!!!!!!!!!!!
            out.print("<td>");
            out.print("");
            out.print("</td>");

            out.print("<td>");
            out.print(a.getValidity());
            out.print("</td>");

            out.print("<td>");
            out.print(a.getAddDate());
            out.print("</td>");

            out.print("<td>");
            out.print(modifyLink(a.getId()));
            out.print("</td>");

            out.print("<td>");
            out.print(removeLink(a.getId()));
            out.print("</td>");

            out.print("</tr>");
        }

    %>

</table>

<div>
    <table>
        <tr>
            <td>
                <form action="add_article.jsp">
                    <input type="submit" value="Add article">
                </form>
            </td>
            <td>
                <form action="save_to_file.jsp">
                    <input type="submit" value="Save to file">
                </form>
            </td>
            <td>
                <form action="load_from_file.jsp">
                    <input type="submit" value="Load from file">
                </form>
            </td>
        </tr>
    </table>
</div>

</body>
</html>
