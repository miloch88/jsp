<%@ page import="store.Article" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.util.Comparator" %>

<%--Dodaje linki na początku strony--%>
<%--<table>--%>
    <%--<tr>--%>
        <%--<td><a href="articles_list.jsp">Article List</a> </td>--%>
        <%--<td><a href="add_article.jsp">Add article</a> </td>--%>
    <%--</tr>--%>
<%--</table>--%>

<%!
    public List<Article> getList(HttpSession session){
    List<Article> articles;
    if (session.getAttribute("article_list") != null) {
    articles = (List<Article>) session.getAttribute("article_list");
    } else {
    articles = new ArrayList<>();
    }

    Collections.sort(articles, new Comparator<Article>() {
        @Override
        public int compare(Article article1, Article article2) {
            if (article1.getId() > article2.getId()) {
                return 1;
            } else if (article1.getId() < article2.getId()) {
                return -1;
            }

            return 0;
        }
    });

    return articles;}

%>