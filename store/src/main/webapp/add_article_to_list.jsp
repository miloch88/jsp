<%@ page import="store.Article" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 27.09.2018
  Time: 09:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Ass Article To List</title>
</head>
<body>

<%! int licznik = 0; %>
<%
    //Importujemy klasę Article
    Article article = new Article();

    //Pobieramy wartości z formularza
    article.setName(request.getParameter("name"));
    article.setQuantity(Integer.parseInt(request.getParameter("quantity")));
//    article.setType(TYPE.valueOf(request.getParameter("type")));
    article.setValidity(LocalDate.parse(request.getParameter("validity")));
    article.setAddDate(LocalDate.now());

    //Walidacja
    boolean isOk = true;
    if (article.getName().isEmpty()) {
        isOk = false;
    }
    if (article.getQuantity() < 0) {
        isOk = false;
    }

    if (!isOk) {
        response.sendRedirect("add_article.jsp" +
                "?name=" + request.getParameter("name") +
                "&quantity=" + request.getParameter("quantity") +
                "&validity=" + request.getParameter("validity"));
//        response.sendRedirect("add_article.jsp");
    } else {

        //Dodajemy licznik po walidacji
        article.setId(licznik++);
        //Dodajemy do listy
        List<Article> articles;
        if (session.getAttribute("article_list") != null) {
            articles = (List<Article>) session.getAttribute("article_list");
        } else {
            articles = new ArrayList<>();
        }

        articles.add(article);

        //Dodajemy do sesji
        session.setAttribute("article_list", articles);
        response.sendRedirect("articles_list.jsp");
    }

%>

</body>
</html>
