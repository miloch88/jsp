package store;

import java.time.LocalDate;

public class Article {

    private String name;
    private int id;
    private LocalDate validity;
    private int quantity;
    private LocalDate addDate;
    private TYPE type;

    public Article() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getValidity() {
        return validity;
    }

    public void setValidity(LocalDate validity) {
        this.validity = validity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public LocalDate getAddDate() {
        return addDate;
    }

    public void setAddDate(LocalDate addDate) {
        this.addDate = addDate;
    }

//    public TYPE getType() {
//        return type;
//    }
//
//    public void setType(TYPE type) {
//        this.type = type;
//    }

    public String writeToFile(){
        StringBuilder builder = new StringBuilder();

        builder.append(id).append(";")
                .append(name).append(";")
                .append(quantity).append(";")
                .append(validity).append(";")
                .append(addDate).append(";");

        return builder.toString();
    }

    public void loadFromFile(String line){
        String[] value = line.split(";");
        setId(Integer.parseInt(value[0]));
        setName(value[1]);
        setQuantity(Integer.parseInt(value[2]));
        setValidity(LocalDate.parse(value[3]));
        setAddDate(LocalDate.parse(value[4]));
    }

    @Override
    public String toString() {
        return "Article{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", validity=" + validity +
                ", quantity=" + quantity +
                ", addDate=" + addDate +
//                ", type=" + type +
                '}';
    }
}
