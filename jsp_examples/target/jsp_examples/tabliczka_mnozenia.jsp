<%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 24.09.2018
  Time: 19:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tabliczka mnożenia</title>
</head>
<body>

    <h1>Formularz wielkości tabliczki mnożenia: </h1>

    <form action="tabliczka_mnozenia.jsp" method="get">
        <input type="number" name="wielkosc">
        <input type="submit" value="Wygeneruj">
    </form>

    <hr>

    <h2> Tabliczka mnożenia: </h2>
    <%
        String parametrWielkosc = request.getParameter("wielkosc");
        try{
            Integer wielkosc = Integer.parseInt(parametrWielkosc);
            out.print(" Tabliczka wilekości ma wielkość " + wielkosc);
            out.print("<p> </p>");
            //wyświetl tabliczkę mnżenia o wielkosći wielkość * wielkość
            out.print("<table>");
            out.print("<tr>"); // rozpoczęcie nowego wiersza
            for (int i = 0; i <= wielkosc; i++) {
                for (int j = 0; j <= wielkosc ; j++) {
                    out.print("<td>" + i*j + "</td>"); //<td> nowa komórka
                }
                out.print("</tr>");
            }
            out.print("</tablica>");

        }catch (NumberFormatException nfe){
            out.print("Błąd, nie porawna wartość paramaetru!");
        }
    %>

</body>
</html>
