<%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 24.09.2018
  Time: 18:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tabliczka mnożenia static</title>
</head>
<body>

<table>

<%
    //swtórz tabliczkę mnożenia (statyczną)
    out.print("<tr>"); // rozpoczęcie nowego wiersza
    for (int i = 0; i <= 50; i++) {
        for (int j = 0; j <= 50 ; j++) {
            out.print("<td>" + i*j + "</td>"); //<td> nowa komórka
        }
        out.print("</tr>");
    }
%>

</table>

</body>
</html>
