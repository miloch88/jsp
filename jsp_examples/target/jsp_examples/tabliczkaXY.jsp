<%--
  Created by IntelliJ IDEA.
  User: Miloch
  Date: 24.09.2018
  Time: 19:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tabliczka XY</title>
</head>
<body>

    <h1>Podaj wartości</h1>
    <form action="tabliczkaXY.jsp" method="post">
        <input type="number" name="x">
        <input type="number" name="y">
        <input type="submit" value="Wygeneruj">
    </form>


    <hr>
    <h2> Tabliczka mnożenia: </h2>
    <%
        String wartoscX = request.getParameter("x");
        String wartoscY = request.getParameter("y");

        try{
            Integer wielkoscX = Integer.parseInt(wartoscX);
            Integer wielkoscY = Integer.parseInt(wartoscY);
            out.print("<p></p><table><tr>");
            for (int i = 0; i <= wielkoscX; i++) {
                for (int j = 0; j <= wielkoscY; j++) {
                    out.print("<td>" + i*j + "</td>"); //<td> nowa komórka
                }
                out.print("</tr>");
            }
            out.print("</tablica>");
        }catch (NumberFormatException nfe){
            out.print("Błąd ");
        }
    %>

</body>
</html>
