package com.javagda14.servlet.todos.service;

import javax.inject.Singleton;
import java.util.HashMap;
import java.util.Map;

/**
 * Dependency manager to klasa przechowująca nasze bean'y. Przechowuje w mapie wszystkie obiekty gdzie kluczem jest
 * typ obiektu a wartością jego instancja. Dzięki temu że jest to singleton, mamy możliwość pobrania bean'u
 * z dowolnego miejsca w kodzie.
 */
public class DependencyManager {
    private static DependencyManager ourInstance = new DependencyManager();

    public static DependencyManager getInstance() {
        return ourInstance;
    }

    private DependencyManager() {
        // w konstruktorze musimy dodać wszystkie beany które manager będzie przechowywać
        registerBean(UserService.class, new UserServiceImpl());
        registerBean(TaskService.class, new TaskServiceImpl());
    }

    private Map<Class<?>, Object> beanMap = new HashMap<>();

    //DependencyManager.getInstance().getBean(UserService.class);
    public <T> T getBean(Class<T> className) {
        return (T) beanMap.get(className);
    }

    private void registerBean(Class<?> className, Object o) {
        beanMap.put(className, o);
    }
}
