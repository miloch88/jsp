package com.javagda14.servlet.todos.controller;

import com.javagda14.servlet.todos.model.TodoTask;
import com.javagda14.servlet.todos.service.DependencyManager;
import com.javagda14.servlet.todos.service.TaskService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/task/add")
public class TaskAddController extends HttpServlet {
    private int licznik = 0;
    private TaskService taskService;

    public TaskAddController() {
        taskService = DependencyManager.getInstance().getBean(TaskService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/task/task_form.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String title = req.getParameter("title");
        String content = req.getParameter("content");
        boolean done = req.getParameter("done")== null ? false : req.getParameter("done").equals("on");

        if (title.isEmpty() || content.isEmpty() ) {
            req.setAttribute("error_message", "Title and content cannot be empty!");

            req.getRequestDispatcher("/task/task_form.jsp").forward(req, resp);
            return;
        }

        TodoTask newTask = new TodoTask(licznik++, title, content, done);

        // dodaje do serwisu
        taskService.addTask(newTask);

        resp.sendRedirect("/task/list");
    }
}
