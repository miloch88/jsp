package com.javagda14.servlet.todos.controller;

import com.javagda14.servlet.todos.service.DependencyManager;
import com.javagda14.servlet.todos.service.TaskService;
import com.javagda14.servlet.todos.service.UserService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/task/remove")
public class TaskRemoveController extends HttpServlet {
    @Inject
    private TaskService taskService;

    public TaskRemoveController() {
        taskService = DependencyManager.getInstance().getBean(TaskService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(req.getParameter("id")!= null){
            int id = Integer.parseInt(req.getParameter("id"));

            taskService.removeTaskWithId(id);
        }

        resp.sendRedirect(req.getContextPath() +"/task/list");
    }
}
