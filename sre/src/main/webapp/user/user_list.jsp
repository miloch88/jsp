<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 10/3/18
  Time: 8:36 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User list</title>
</head>
<body>
<h2>User List:</h2>
<c:out value="#{licznik}"></c:out>
<%--<c:out value="${userList}"/>--%>
<table>
    <thead>
    <th>Id</th>
    <th>Login</th>
    <th>Password</th>
    <th>Action remove</th>
    <th>Action modify</th>
    </thead>
    <tbody>
    <%--zmienna userList bierze się z klasy UserListController
        i jest ładowany jako atrybut
        --%>
    <c:forEach items="${userList}" var="user">
        <tr>
            <td>
                <c:out value="${user.id}"/>
            </td>
            <td>
                <c:out value="${user.login}"/>
            </td>
            <td>
                <c:out value="${user.password}"/>
            </td>
            <td>
                <a href="/user/remove?id=<c:out value="${user.id}"/>">Remove</a>
            </td>
            <td>
                <a href="/user/modify?id=<c:out value="${user.id}"/>">Modify</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
